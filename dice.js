class Dice {

    constructor(sides) {
        this.Sides = this.DefineSides(sides);
    }

    DefineSides(sides) {
        return Array.from({length: sides}, (_, i) => i + 1);
    }

    Roll() {
        this.Result = this.Sides[this.Sides.length * Math.random() | 0];
        return this.Result;
    }

    toString() {
        return this.Sides;
    }
}

module.exports = Dice;