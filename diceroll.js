const Dice = require("./dice");

class DiceRoll {

    constructor(numDice,diceType) {
        
        this.Dice = new Array();

        for (let index = 0; index < numDice; index++) {
            this.Dice.push(new Dice(diceType));            
        }
    }

    RollAllDice() {
        this.Dice.forEach(dice => {
            dice.Roll();
        });
    }

    GetReadableResult() {
        let output  = "";      
        let modifiedResult,sucesses;

        //Checks if target number is set
        if(this.Target) {
            sucesses = this.CountSucesses();
            output += " **"+sucesses+"** "
        }

        let numDice = this.Dice.length;

        //Turning on cursive
        output += "*";
        //Adding parahantise for sucess/fail
        (this.Target) ? output += "(" : false;

        //Looping thru dice
        for (let i = 0; i < numDice; i++) {
            modifiedResult = parseFloat(this.Dice[i].Result)+parseFloat(this.Modifier||0);
            output += modifiedResult;

            if(i == sucesses-1 && i != numDice-1) {
                output += " / ";
            } else if (i != numDice-1) {
                output += ", ";
            }
        }
        
         //Adding parahantise for sucess/fail
         (this.Target) ? output += ")" : false;
        //Turning off cursive
        output += "*";


        // //removing the trailing comma
        // output = output.slice(0, -2)
        return output;
    }

    TestProbability(elements) {
        let prob = new Array(elements).fill(0);
        let result;

        this.Dice.forEach(dice => {
            result = dice.Roll();
            prob[result-1] += 1;
        });

        return prob;        
    }

    CountSucesses() {
        let successes = 0;

        this.Dice.forEach(dice => {
           (parseFloat(dice.Result)+(parseFloat(this.Modifier)||0) >= this.Target) ? successes++ : false; 
        })

        return successes;
    }

    Sort() {
        this.Dice.sort(function(a, b) {
            return b.Result - a.Result;
          });          
    }

    SetTargetNumber(target) {
        this.Target = target;
    }

    SetModifider(modifier) {
        this.Modifier = modifier;
    }

    GetDiceName() {

    }

    // toString() {
    //     let output = Array();
    //     this.Dice.forEach(dice => {
    //         outpt.push = dice.Result;
    //     });

    //     return output;
    // }
}

module.exports = DiceRoll;