//  "use strict";

const Discord = require('discord.js');

const DiceRoll = require("./diceroll.js");
const SulDice = require("./suldice.js");
const Dice = require("./dice.js");


const globalConfig = require('./config.json');
const client = new Discord.Client();
var request = require("request");

// setInterval(function(){ Rand(0,2); }, 1000);


client.on('ready', () => {
    PrintToConsle("Starting " + client.user.tag);

    //List all connected servers
    let msg = "Connected servers: ";

    client.guilds.forEach(element => {
        msg += "\n\t" + element.name;
        msg += ` (${element.id})`;
    });

    // let testmsg = "/bot 1d6";
    // console.log(ParseCommand(testmsg,"/bot"))


    PrintToConsle(msg);
})

client.on('message', msg => {
    //Gets current cofnig file, based on server
    let serverConfig = GetConfig(msg);

    //Gets the experssion to listen for, makes lower to avoid case problems.
    let lowCaseExpr = serverConfig.prefix.toLowerCase();

    //Check if messeage starts with the pattern the bot listens to
    if (msg.content.toLowerCase().startsWith(lowCaseExpr)) {
        PrintToConsle(msg.author.username + " (" + serverConfig.name + ")" + ": " + msg.content);

        //Defines roles allowed to use the command
        // var allowerdRole = msg.guild.roles.find("name","Admin");;

        let formatedMsg = msg.content.toLowerCase();
        let parsedCommand = ParseCommand(formatedMsg, lowCaseExpr);
        ChooseCommand(parsedCommand, msg);
    }
});

client.login(globalConfig.botToken);

function ChooseCommand(command, msg) {
    let max, paramName, response, result, diceType,diceNum;
    let roll;
    let rspnContent = "";

    switch (command.command) {
        case "suldice":
            dice = new SulDice();
            result = dice.Roll();
            RespondToDiscord(msg, result);
            break;

        case "testprob":
            diceNum = 100000;
            diceType = 10;

            roll = new DiceRoll(diceNum, diceType);
            result = roll.TestProbability(diceType);

            rspnContent += diceNum + "d" + diceType + ":\n";
            result.forEach(function (value, i) {
                rspnContent += i+1 + ": " + value + "\n";
            });

            RespondToDiscord(msg,rspnContent);
            
            break;

        case "generic":
            diceNum = command.diceNum;
            diceType = command.diceType;

            roll = new DiceRoll(diceNum, diceType);
            roll.RollAllDice();
            roll.Sort();

            if (command.add != 0) {
                roll.SetModifider(command.add);
            }

            if(command.target != 0) {
                roll.SetTargetNumber(command.target);
            }          

            rspnContent = command.raw+ ":" + roll.GetReadableResult(command.add);
            RespondToDiscord(msg, rspnContent);
            break;

        default:
            //Checks config for default behavoir based on server
            PrintToConsle("Command not found, using default");
            let defaultCommand = GetConfig(msg).default;
            let defaultParsedCommand = ParseCommand(defaultCommand.toLowerCase(), "");
            ChooseCommand(defaultParsedCommand, msg);
            break;
    }
}

function GetConfig(msg) {
    let config;

    if (msg.channel.type == "dm") {
        config = globalConfig.dm;
    }
    else {
        config = globalConfig[msg.guild.id]; //Checks for discord serverid, error if undifined (if msg is from a PM)

        if (!config) { //Checks if preset config exists for server, otherwise chooses default
            config = globalConfig.default
        }
    }

    return config;
}

function Rand(min, max) {
    let rand = Math.round(Math.random() * max) + min
    // console.log("Max: " + max + "\nMin: " + min + "\nResult: " + rand + "\n");
    return rand;
}

/**
 * Parses a discord message into a easier format.
 *
 * @param {*} msg Discord message from the pattern 
 * @param {*} listExpr Expression the bot listens for
 * @returns array, first value is the command second value is a key.value dictionary of the params
 */
function ParseCommand(msg, listExpr) {
    //Remvove the expresson
    let formatedCommand = msg.replace(listExpr, "");
    let outputCommand = new Object();
    formatedCommand.trim();

    outputCommand.raw = formatedCommand;

    //Splits on given delimtiter
    formatedCommand = formatedCommand.split(" ");
    //Removes any empty element
    formatedCommand = formatedCommand.filter(n => n);

    //Removes whitespace from all elements
    // trimedFormCommand = formatedCommand.map(element => element.trim());

    //Checks if first element matches known dice pattern (eg 1d6)
    let genDRegex = new RegExp("\\d+d\\d+");
    let addDRegex = new RegExp("\\+\\d+");
    let tarDRegex = new RegExp("t\\d+");
    // let addDRegex = new RegExp("\\+[0-9]");
    if (genDRegex.test(formatedCommand[0])) {
        // let splitDice = formatedCommand[0].split(/t|d|[\+]/);

        let diceData = formatedCommand[0].match(genDRegex);
        diceData = diceData[0].split("d");

        //Checks for +k
        let diceAdd;
        if (addDRegex.test(formatedCommand[0])) {
            diceAdd = formatedCommand[0].match(addDRegex);
            diceAdd = diceAdd[0].split("\+")[1];
        }

         //Checks for targetnumber
         let diceTar;
         if (tarDRegex.test(formatedCommand[0])) {
            diceTar = formatedCommand[0].match(tarDRegex);
            diceTar = diceTar[0].split("t")[1];
         }

        outputCommand.command = "generic";
        outputCommand.diceNum = diceData[0];
        outputCommand.diceType = diceData[1];
        outputCommand.add = diceAdd || 0;
        outputCommand.target = diceTar || 0;
    } else {
        outputCommand.command = formatedCommand[0];
    }

    return outputCommand;
}


//Checks if user has a given role
function UserHasRole(msg, roleName) {
    return msg.member.roles.some(role => role.name === roleName)
}

//Sens message as a respons to command in discord
function RespondToDiscord(msg, msgContent) {
    try {
        msg.reply(msgContent);
        PrintToConsle("Sending response to Discord: '" + msgContent + "'");
    }
    catch (error) {
        PrintToConsle(error);
    }
}

//Prints formated messege to console
function PrintToConsle($msg) {
    let date = new Date();
    let parseMsg = date.toLocaleDateString() + " " + date.toLocaleTimeString();
    parseMsg += ": " + $msg;
    console.log(parseMsg);
}
