const Dice = require("./dice.js");

class SulDice extends Dice {

    constructor(sides) {
        super(sides);
    }

    DefineSides(sides) {
        return [
            "Lyckas",
            "Misslyckas",
            "Fantastiskt lyckat",
            "Fantastiskt misslyckat",
            "Knappt lyckat",
            "Knappt misslyckat"
        ];
    }

}

module.exports = SulDice;